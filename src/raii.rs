struct ToDrop;

use std::fmt;

impl fmt::Display for ToDrop {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", "Hi!")
    }
}


pub fn main() {
    println!("Start!");
    let x = ToDrop;
    println!("{}", &x);
    println!("End!");
}

impl Drop for ToDrop {
    fn drop(&mut self) {
        println!("Drop ToDrop")
    }
}
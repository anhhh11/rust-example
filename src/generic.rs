use std::marker::PhantomData;

#[derive(PartialEq)]
struct PhantomTuple<A, B>(A, PhantomData<B>);

trait Contains {
    type A;
    type B;
    fn contains(&self, &Self::A, &Self::B) -> bool;
    fn first(&self) -> &Self::A;
    fn second(&self) -> &Self::B;
}

struct Container(i32, i32);

impl Contains for Container {
    type A = i32;
    type B = i32;
    fn contains(&self, n1: &i32, n2: &i32) -> bool {
        (&self.0 == n1) && (&self.1 == n2)
    }

    fn first(&self) -> &i32 {
        &self.0
    }

    fn second(&self) -> &i32 {
        &self.1
    }
}

pub trait SpecialAdd<RHS = Self> {
    type Output;
    fn special_add(&self, rhs: &RHS) -> Self::Output;
}

impl SpecialAdd for i32 {
    type Output = i32;

    fn special_add(&self, rhs: &i32) -> Self::Output {
        return self + rhs + 100;
    }
}

pub fn main() {
    let x = Container(10, 20);
    let a: PhantomTuple<i32, u8> = PhantomTuple(20i32, PhantomData);
    let b: PhantomTuple<i32, u8> = PhantomTuple(20i32, PhantomData);
    println!("{}, {}, {}", x.contains(&10, &20), x.first(), x.second());
    println!("{}", a == b);
    println!("{}", 1.special_add(&1));
}
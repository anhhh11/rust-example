/*
Entry point of the program
Description: Program print the addition of number to screen
*/

#[allow(unused_imports)]
use std::fmt;
use std::num;
use std::mem;

#[allow(dead_code)]
const SYSTEM_NAME: &'static str = "test";

mod todo;
mod generic;
mod raii;

#[derive(Debug)]
struct ProxyAddress<'a> {
    ip: &'a str,
    port: u16,
    last_response_time: Option<f32>,
    average_response_time: Option<f32>,
    is_used: bool
}

impl<'a> From<u16> for ProxyAddress<'a> {
    fn from(p: u16) -> Self {
        let mut x = create_default_proxy();
        x.port = p;
        x
    }
}

impl<'a> ToString for ProxyAddress<'a> {
    fn to_string(&self) -> String {
        format!("{}:{}", self.ip.to_string(), self.port.to_string())
    }
}

//impl<'a> std::fmt::Display for ProxyAddress<'a> {
//    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//        write!(f, "{ip}:{port}", ip = self.ip, port = self.port)
//    }
//}


#[derive(Debug)]
enum RequestEvent<'a> {
    Initialize(&'a str),
    Connected(&'a str),
    SendData(&'a [u8]),
    SentOk(u32)
}

#[derive(Debug)]
enum State {
    Ready = 0x00,
    Sending = 0x01,
    Error = 0x02,
    Closed = 0x03
}

fn inspect_request(r: RequestEvent) {
    use RequestEvent::*;
    match r {
        Initialize(str) => println!("{:?}", str),
        Connected(str) => println!("{:?}", str),
        SendData(bs) => println!("{:?}", bs),
        SentOk(b) => println!("{:?}", b),
    }
}

fn create_default_proxy<'a>() -> ProxyAddress<'a> {
    ProxyAddress {
        ip: "0.0.0.0",
        port: 0,
        last_response_time: None,
        average_response_time: None,
        is_used: false
    }
}


#[allow(dead_code)]
type Byte = u8;

fn main() {
    generic::main();
    raii::main();
}

#[allow(dead_code)]
fn old_old_main() {
    let p = ProxyAddress::from(10);
    println!("{:?}", p);
    let p1: ProxyAddress = 20.into();
    println!("{:?}", p1);
    print!("{}", p.to_string());

    let mut names = vec!["Bob", "Frank", "Ferris"];
    for name in names.iter_mut() {
        *name = "hi";
        match name {
            &mut "Ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }
    names = vec!["a", "b", "c"];
    println!("{:?}", names);
}

#[allow(dead_code)]
fn old_main() {
    let mut _count_down = 1_000_000_000;
    let pa = ProxyAddress {
        ip: "127.0.0.1",
        port: 8080,
        last_response_time: None,
        average_response_time: None,
        is_used: false
    };
    print_number_all_form(pa.port);
    debug(&pa);
    println!("{}", pa.to_string());
    println!("100 * 2 = {}", fast_mul_two(100));
    println!("100 / 2 = {}", fast_div_two(100));
    print_number_all_form(encode_number(11, 120));
    println!("{:?}", ip4(&pa.ip));
    println!("{:?}", ip4("a.b"));
    let my_proxies: [ProxyAddress; 3] = [
        create_default_proxy(),
        create_default_proxy(),
        create_default_proxy(),
    ];
    println!("{:?}", my_proxies);
    println!("Size my proxies: {}", my_proxies.len());
    println!("Mem used by my proxies: {}", mem::size_of_val(&my_proxies));
    println!("{:?}", &my_proxies[0..1]);
    inspect_request(RequestEvent::Initialize("127.0.0.1"));
    inspect_request(RequestEvent::Connected("127.0.0.1"));
    inspect_request(RequestEvent::SendData(&[1, 2, 3]));
    inspect_request(RequestEvent::SentOk(100));
    println!("{}", State::Closed as i32);
    println!("{}", State::Error as i32);
    println!("{}", State::Ready as i32);
    println!("{}", State::Sending as i32);

    let mut list = List::new();
    list = list.prepend(10);
    list = list.prepend(20);
    list = list.prepend(20);
    println!("{} : {}", list.stringify(), list.len());
    todo::add_todo("Hello");
}

fn print_number_all_form<T>(x: T) -> () where
    T: std::fmt::Debug,
    T: std::fmt::Display,
    T: std::fmt::Binary,
    T: std::fmt::Octal,
    T: std::fmt::LowerHex {
    println!("Raw value {x:#?}, in dec = {x:6}, in hex = 0x{x:x}, in oct = 0o{x:o}, in bin = 0b{x:b}", x = x);
}

fn ip4(s: &str) -> Result<(u8, u8, u8, u8), num::ParseIntError> {
    let t: Vec<_> = s.split('.').collect();
    Ok((
        t[0].parse::<u8>()?,
        t[1].parse::<u8>()?,
        t[2].parse::<u8>()?,
        t[3].parse::<u8>()?,
    ))
}

fn debug<T: std::fmt::Debug>(obj: &T) -> () {
    println!("{:#?}", obj)
}

fn fast_mul_two<T: std::ops::Shl<i32, Output=T>>(x: T) -> T {
    x << 1
}

fn fast_div_two<T: std::ops::Shr<i32, Output=T>>(x: T) -> T {
    x >> 1
}

fn encode_number<T: std::ops::BitAnd<T, Output=T> + std::ops::BitOr<T, Output=T>>(a: T, b: T) -> T
    where T: std::ops::BitXor<T, Output=T>,
          T: std::ops::Not<Output=T>,
          T: Copy
{
    return !(((a & b) | a) ^ b)
}

use List::*;

enum List {
    Cons(u32, Box<List>),
    Nil
}

impl List {
    fn new() -> List {
        Nil
    }
    fn prepend(self, elem: u32) -> List {
        Cons(elem, Box::new(self))
    }
    fn len(&self) -> u32 {
        match *self {
            Cons(_, ref tail) => 1 + tail.len(),
            Nil => 0
        }
    }
    fn stringify(&self) -> String {
        match *self {
            Cons(head, ref tail) => {
                format!("{}, {}", head, tail.stringify())
            }
            Nil => {
                format!("Nil")
            }
        }
    }
}
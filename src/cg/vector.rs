pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32
}
impl Add<Vector3,Vector3> for Vector3 {
    fn add(&self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z
        }
    }
}
impl Mul<Vector3,Vector3> for Vector3 {
    fn mul(&self, f: &f32) -> Vector3 {
         Vector3 {
             x: self.x * *f,
             y: self.y * *f,
             z: self.z * *f
         }
    }
}
pub trait Sqrtable {
    fn sqrt(self) -> Self;
}
//use super::super::vector::Vect3;
use cg::sqrtable::Sqrtable;
use cg::vector::Vect3;

impl Sqrtable for f32 {
    fn sqrt(self) -> Self{
        f32::sqrt(self)
    }
}